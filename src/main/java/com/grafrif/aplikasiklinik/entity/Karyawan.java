package com.grafrif.aplikasiklinik.entity;

import com.grafrif.aplikasiklinik.entity.config.User;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
public class Karyawan {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @NotNull
    private String name;

    @NotNull
    private String alamat;

    private String email;

    private String nomorTelpon;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;


}
