package com.grafrif.aplikasiklinik.entity;


import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;

@Entity
@Data
public class RekamMedis {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pasien")
    private Pasien pasien;

    @ManyToOne
    @JoinColumn(name = "id_dokter")
    private Dokter dokter;

    private LocalDate tanggalPeriksa;

    private String diagnosis;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;
}
