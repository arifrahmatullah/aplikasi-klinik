package com.grafrif.aplikasiklinik.entity;

public enum StatusRecord {
    ACTIVE, NONACTIVE, DELETED
}
