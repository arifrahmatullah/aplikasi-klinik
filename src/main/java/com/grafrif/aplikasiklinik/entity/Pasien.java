package com.grafrif.aplikasiklinik.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;

@Entity
@Data
public class Pasien {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String nama;

    @NotNull
    private String alamat;

    private String nomorTelpon;

    private Integer umur;

    private LocalDate tanggalLahir;

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;


}
