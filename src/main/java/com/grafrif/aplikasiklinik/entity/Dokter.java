package com.grafrif.aplikasiklinik.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
public class Dokter {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    private String spesialisasi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

}
