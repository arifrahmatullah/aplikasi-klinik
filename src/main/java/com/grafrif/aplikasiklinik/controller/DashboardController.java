package com.grafrif.aplikasiklinik.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {

    @GetMapping("/")
    public String started(){
        return "login";
    }

    @GetMapping("/dashboard")
    public String dashboard(Model model){
        model.addAttribute("dashboard", "active");
        return "dashboard/index";
    }
}
