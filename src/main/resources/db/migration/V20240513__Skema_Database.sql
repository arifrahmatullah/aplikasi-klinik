create table s_user(
    id varchar(36)not null ,
    username varchar(36) not null ,
    password varchar(36) not null ,
    id_role varchar(36) not null,
    status varchar(30),
    primary key (id)
);

create table s_role(
    id varchar(36) not null ,
    name varchar(36) not null ,
    description varchar(255),
    primary key (id)
);

create table s_role_permission(
    id_role varchar(36) not null,
    id_permission varchar(36) not null
);

create table s_permission(
    id varchar(36) not null,
    permission_label varchar(36),
    permission_value varchar(255),
    primary key (id)
);

create table karyawan(
    id varchar(36) not null ,
    id_user varchar(36) not null ,
    nama varchar(50) not null ,
    alamat varchar(36) not null ,
    email varchar(36),
    nomor_telpon varchar(13),
    nik varchar(16),
    status varchar(20),
    primary key (id)
);

CREATE TABLE dokter (
    id varchar(36) not null,
    id_karyawan varchar(36),
    spesialisasi varchar(100),
    status varchar(20),
    primary key (id)
);

create table pasien(
    id varchar(36) not null ,
    nama varchar(50) not null ,
    alamat varchar(255) not null ,
    nomor_telpon varchar(13),
    umur int(3),
    tanggal_lahir date,
    jenis_kelamin varchar(10),
    status varchar(20),
    primary key (id)
);

create table rekam_medis(
    id varchar(36) not null ,
    id_pasien varchar(36) not null ,
    id_dokter varchar(36) not null ,
    tanggal_periksa date not null ,
    diagnosis varchar(1000),
    status varchar(20),
    primary key (id)
);